<?php include("header.php"); ?>
<!DOCTYPE html>
<html>
    <head>
        <title>RAAN - MERCH</title>
    </head>

    <body>

    <!--METTRE UN SLIDER OU UN ARTICLE-->

    <br/><br/>
    <h2> MES DERNIERS PRODUITS </h2>
    <div class="container w-screen merch_container">
        <section class="row merch">

            <?php
                $products = $db->query('SELECT * FROM merch ORDER BY id DESC LIMIT 4');
                foreach ($products as $product): 

            ?>
            <div class="col-lg-3">
                <img class="col-lg-12" src="./pic/<?php echo $product->image; ?>" alt="<?php echo $product->descritpion; ?>" />
                <br/><br/>
                <p><?php echo $product->description; ?></p>
                <p>Prix:<?php echo $product->prix . "€"; ?></p>
                <a class="addPanier" href="addpanier.php?id=<?= $product->id; ?>"> 
                    <button class="btn btn-primary center-block" type="button">Ajouter au panier</button> 
                </a>
            </div>

            <?php
                endforeach;
            ?>

        </section>
    </div>
        </section>
    </div>

    <br/><br/>

    <h2> VETEMENTS </h2>
    <div class="container w-screen merch_container">
        <section class="row merch">

        <?php
                $products = $db->query('SELECT * FROM merch WHERE type=\'tshirt\'');
                foreach ($products as $product): 

            ?>
            <div class="col-lg-3">
                <img class="col-lg-12" src="./pic/<?php echo $product->image; ?>" alt="<?php echo $product->descritpion; ?>" />
                <br/><br/>
                <p><?php echo $product->description; ?></p>
                <p>Prix:<?php echo $product->prix . "€"; ?></p>
                <a class="addPanier" href="addpanier.php?id=<?= $product->id; ?>"> 
                    <button class="btn btn-primary center-block" type="button">Ajouter au panier</button> 
                </a>
            </div>

            <?php
                endforeach;
            ?>

        </section>
    </div>

    <br/><br/>

    <h2> CD </h2>
    <div class="container w-screen merch_container">
        <section class="row merch">

        <?php
                $products = $db->query('SELECT * FROM merch WHERE type=\'cd\'');
                foreach ($products as $product): 

            ?>
            <div class="col-lg-3">
                <img class="col-lg-12" src="./pic/<?php echo $product->image; ?>" alt="<?php echo $product->descritpion; ?>" />
                <br/><br/>
                <p><?php echo $product->description; ?></p>
                <p>Prix:<?php echo $product->prix . "€"; ?></p>
                <a class="addPanier" href="addpanier.php?id=<?= $product->id; ?>"> 
                    <button class="btn btn-primary center-block" type="button">Ajouter au panier</button> 
                </a>
            </div>

            <?php
                endforeach;
            ?>

        </section>
    </div>

    <br/><br/>

    <h2> VINYLS </h2>
    <div class="container w-screen merch_container">
        <section class="row merch">

        <?php
                $products = $db->query('SELECT * FROM merch WHERE type=\'vinyl\'');
                foreach ($products as $product): 

            ?>
            <div class="col-lg-3">
                <img class="col-lg-12" src="./pic/<?php echo $product->image; ?>" alt="<?php echo $product->descritpion; ?>" />
                <br/><br/>
                <p><?php echo $product->description; ?></p>
                <p>Prix:<?php echo $product->prix . "€"; ?></p>
                <!-- <form method="post" action="addpanier.php?id=<?= $product->id; ?>">
                  <input type="submit" class="addPanier btn btn-primary center-block" value="Ajouter au panier"/>
                </form> -->

                <a class="addPanier" href="addpanier.php?id=<?= $product->id; ?>"> 
                    <button class="btn btn-primary center-block" type="button">Ajouter au panier</button> 
                </a>
            </div>

            <?php
                endforeach;
            ?>

        </section>
    </div>

   
    <?php include("footer.php"); ?>
    </body>
</html>