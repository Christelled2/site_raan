<?php include("header.php"); ?>
<!DOCTYPE html>
<html>
    <head>
        <title>RAAN - BIOGRAPHIE</title>
    </head>

    <body>

    <!-- CHANGER LES IMAGES DU CAROUSEL POUR METTRE DES PHOTOS DE RAAN -->

    <div id="carousel" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
            <li data-target="#carousel" data-slide-to="0" class="active"></li>
            <li data-target="#carousel" data-slide-to="1"></li>
            <li data-target="#carousel" data-slide-to="2"></li>
            <li data-target="#carousel" data-slide-to="3"></li>
            <li data-target="#carousel" data-slide-to="4"></li>
        </ol>
        <div class="carousel-inner">
            <div class="carousel-item active">
                <img class="d-block w-70" src="./pic/slide1.jpg" alt="First slide">
            </div>

            <div class="carousel-item">
                <img class="d-block w-70" src="./pic/slide2.jpg" alt="Second slide">
            </div>

            <div class="carousel-item">
                <img class="d-block w-70" src="./pic/slide3.jpg" alt="Third slide">
            </div>

            <div class="carousel-item">
                <img class="d-block w-70" src="./pic/slide4.jpg" alt="Fourth slide">
            </div>

            <div class="carousel-item">
                <img class="d-block w-70" src="./pic/slide5.jpg" alt="Fifth slide">
            </div>
        </div>
        <!-- BOUTONS POUR CAROUSEL -->
        <a class="carousel-control-prev" href="#carousel" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>

        <a class="carousel-control-next" href="#carousel" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
        <!-- FIN BOUTONS POUR CAROUSEL -->
    </div>

    <h2>Petit blabla me concernant</h2>

    </br></br>
    <article>
        <p>BLaaaaaaaaaaaablabla premier paragraphe blablabla blaaaaaaaaaaaaaaaaaaaaa
        BLaaaaaaaaaaaablabla premier paragraphe blablabla blaaaaaaaaaaaaaaaaaaaaa
        BLaaaaaaaaaaaablabla premier paragraphe blablabla blaaaaaaaaaaaaaaaaaaaaaBLaaaaaaaaaaaablabla premier paragraphe blablabla blaaaaaaaaaaaaaaaaaaaaa
        BLaaaaaaaaaaaablabla premier paragraphe blablabla blaaaaaaaaaaaaaaaaaaaaa
        BLaaaaaaaaaaaablabla premier paragraphe blablabla blaaaaaaaaaaaaaaaaaaaaa
        BLaaaaaaaaaaaablabla premier paragraphe blablabla blaaaaaaaaaaaaaaaaaaaaa
        </p>

        <p>BLaaaaaaaaaaaablabla pdeuxieme paragraphe blablabla blaaaaaaaaaaaaaaaaaaaaaBLaaaaaaaaaaaablabla pdeuxieme paragraphe blablabla blaaaaaaaaaaaaaaaaaaaaa
        BLaaaaaaaaaaaablabla pdeuxieme paragraphe blablabla blaaaaaaaaaaaaaaaaaaaaaBLaaaaaaaaaaaablabla pdeuxieme paragraphe blablabla blaaaaaaaaaaaaaaaaaaaaa
        BLaaaaaaaaaaaablabla pdeuxieme paragraphe blablabla blaaaaaaaaaaaaaaaaaaaaa
        BLaaaaaaaaaaaablabla pdeuxieme paragraphe blablabla blaaaaaaaaaaaaaaaaaaaaa
        BLaaaaaaaaaaaablabla pdeuxieme paragraphe blablabla blaaaaaaaaaaaaaaaaaaaaa
        BLaaaaaaaaaaaablabla pdeuxieme paragraphe blablabla blaaaaaaaaaaaaaaaaaaaaa
        BLaaaaaaaaaaaablabla pdeuxieme paragraphe blablabla blaaaaaaaaaaaaaaaaaaaaaBLaaaaaaaaaaaablabla pdeuxieme paragraphe blablabla blaaaaaaaaaaaaaaaaaaaaaBLaaaaaaaaaaaablabla pdeuxieme paragraphe blablabla blaaaaaaaaaaaaaaaaaaaaa
        BLaaaaaaaaaaaablabla pdeuxieme paragraphe blablabla blaaaaaaaaaaaaaaaaaaaaa
        </p>
    </article>      

    <?php include("footer.php"); ?>
    </body>
</html>