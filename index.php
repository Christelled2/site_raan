<?php include("header.php"); ?>
<!DOCTYPE html>
<html>
    <head>
        <title>RAAN - ACCUEIL</title>
    </head>

    <body>

            <div id="carousel" class="carousel slide" data-ride="carousel">
                <ol class="carousel-indicators">
                    <li data-target="#carousel" data-slide-to="0" class="active"></li>
                    <li data-target="#carousel" data-slide-to="1"></li>
                    <li data-target="#carousel" data-slide-to="2"></li>
                    <li data-target="#carousel" data-slide-to="3"></li>
                    <li data-target="#carousel" data-slide-to="4"></li>
                </ol>
                <div class="carousel-inner">
                    <div class="carousel-item active">
                        <img class="d-block w-70" src="./pic/slide1.jpg" alt="First slide">
                    </div>

                    <div class="carousel-item">
                        <img class="d-block w-70" src="./pic/slide2.jpg" alt="Second slide">
                    </div>

                    <div class="carousel-item">
                        <img class="d-block w-70" src="./pic/slide3.jpg" alt="Third slide">
                    </div>

                    <div class="carousel-item">
                        <img class="d-block w-70" src="./pic/slide4.jpg" alt="Fourth slide">
                    </div>

                    <div class="carousel-item">
                        <img class="d-block w-70" src="./pic/slide5.jpg" alt="Fifth slide">
                    </div>
                </div>
                <!-- BOUTONS POUR CAROUSEL -->
                <a class="carousel-control-prev" href="#carousel" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>

                <a class="carousel-control-next" href="#carousel" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
                <!-- FIN BOUTONS POUR CAROUSEL -->
            </div>

        <article>
            <h2> BLABLABLA TEST </h2>

            <p>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus nec neque id nisl feugiat pretium. Phasellus eu quam enim. Quisque a hendrerit nibh, nec cursus enim. Phasellus blandit consequat lacus, at cursus tellus iaculis id. Suspendisse potenti. Suspendisse turpis arcu, ornare eget justo in, facilisis venenatis massa. Suspendisse potenti. Pellentesque accumsan, ligula ut cursus ornare, purus tellus fermentum odio, vitae interdum nisl ante in libero. Pellentesque vel odio tincidunt, volutpat arcu ac, pharetra justo. Maecenas sit amet accumsan nunc. Interdum et malesuada fames ac ante ipsum primis in faucibus. Vestibulum lacinia facilisis sapien in dignissim. Fusce mi dolor, elementum et venenatis suscipit, tristique ut neque. Duis eu ex eu eros ornare aliquet. Aliquam ultrices elementum urna at blandit.

Etiam in dui a turpis accumsan lacinia vel eu lectus. In a odio venenatis, semper sem ac, dictum magna. Quisque nisi risus, feugiat quis tellus sed, finibus euismod felis. Fusce condimentum consectetur ligula, in condimentum diam aliquet in. Suspendisse vitae ante viverra, venenatis leo at, cursus purus. Nullam bibendum aliquet orci, finibus dapibus lorem facilisis in. Fusce semper, eros sed varius ullamcorper, ipsum neque lacinia felis, et lacinia lorem nisl ac augue. Ut tincidunt justo vel nibh interdum sagittis. Pellentesque condimentum ante ut metus fringilla sodales. Aenean dolor dui, rhoncus et nisl tincidunt, laoreet vehicula dolor. Sed scelerisque nibh quis porta consequat. 
            </p>
        </article>

        <?php include("footer.php"); ?>


        <!-- JAVASCRIPT POUR LE SLIDE -->
        <script>
        var slideIndex = 0;

        function showSlides() {
            var i;
            var slides = document.getElementsByClassName("mySlides");
            var dots = document.getElementsByClassName("dot");

            for (i = 0; i < slides.length; i++) {
                slides[i].style.display = "none";  
            }

            slideIndex++;

            if (slideIndex > slides.length) {
                slideIndex = 1;
            }  

            for (i = 0; i < dots.length; i++) {
                dots[i].className = dots[i].className.replace(" active", "");
            }

            slides[slideIndex-1].style.display = "block";  
            dots[slideIndex-1].className += " active";
            setTimeout(showSlides, 3000); 
        }


        showSlides();
        </script>
    </body>
</html>