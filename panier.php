<?php include("header.php");
if (isset($_GET['del'])) {
    $panier->del($_GET['del']);
}

if (isset($_GET['minus'])) {
    $panier->minus($_GET['minus']);
}

if (isset($_GET['plus'])) {
    $panier->plus($_GET['plus']);
}
?>
<!DOCTYPE html>
<html>
    <head>
        <title>RAAN - MERCH</title>
    </head>

    <body>

    <!--METTRE UN SLIDER OU UN ARTICLE-->

    <h1>MON PANIER</h1>

    <br/><br/><br/>
    <p>Vous avez <?= $panier->nombre_element_panier(); ?> dans votre panier.</p>

    <div class="col-lg-12 panier">
        <div class="row justify-content-lg-center">
            <div class="col-lg-2">Image</div>
            <div class="col-lg-4">Description</div>
            <div class="col-lg-1">Prix</div>
            <div class="col-lg-1">Quantité</div>
            <div class="col-lg-1">Prix total</div>
            <div class="col-lg-1">Action</div>
        </div>

        <?php 
            //on récupère les id des produits de la session:
            $id_panier = array_keys($_SESSION['panier']);
            if (empty($id_panier)) { //si le panier est vide ...
                $products = array();
            }else{ //si le panier n'est pas vide, faire la requete
                $products = $db->query('SELECT * FROM merch WHERE id IN('.implode(',',$id_panier).')' );
            }
            $prix_total_commande = 0; //pour calculer le prix total du panier, on rajoute à chaque ligne le prix
            
            foreach ($products as $product) :
        ?>
        <div class="row justify-content-lg-center">
            
            <!-- IMAGE -->
            <div class="col-lg-2">
                <img class="col-lg-12" src="./pic/<?php echo $product->image; ?>" alt="<?php echo $product->descritpion; ?>" />
            </div>  

            <!-- Description -->
            <div class="col-lg-4">
                <p><?php echo $product->description; ?></p>
            </div>  

            <!-- Prix -->
            <div class="col-lg-1">
                <p><?php echo $product->prix; ?> €</p>
            </div>  

            <!-- Quantité -->
            <div class="col-lg-1">
                <?php echo $_SESSION['panier'][$product->id] ?>
            </div>
            
            <!-- Prix total -->
            <div class="col-lg-1">
                <?php 
                    //on calcule le prix total en fonction du nombre de produit qu'on commande
                    $total = ($product->prix) * $_SESSION['panier'][$product->id];
                    echo $total . " €"; 
                ?>
            </div>

            <!-- Jeter -->
            <div class="col-lg-1">
            <a href="panier.php?minus=<?php echo $product->id; ?>"> - </a> <a href="panier.php?del=<?php echo $product->id; ?>"> Tout jeter </a> <a href="panier.php?plus=<?php echo $product->id; ?>"> + </a>
            </div> 

        </div>

        <?php
            $prix_total_commande += $total;
            endforeach;
        ?>

    </div>

    <p> <?php 
    if ($prix_total_commande === 0) {
        echo "VOtre panier est vide. Remplissez-le :)";
    } else {
        echo "<br/><br/><br/>Prix total de votre commande:" . $prix_total_commande . "€";
    }
    ?></p>

    <br/><br/><br/>

    <button class="btn btn_paiement btn-primary center-block" type="button"> Procéder au paiement </button>
            
    <?php include("footer.php"); ?>
    </body>
</html>