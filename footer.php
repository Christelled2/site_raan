<!DOCTYPE html>
<html>
    <body>
        <footer>
            <div class="container">
                <div class="row">
                    <div class="reseaux_sociaux col-lg-6">
                        <h4>Retrouvez-moi sur vos réseaux sociaux préférés!</h4>
                        <ul>
                            <li><a href="https://twitter.com/Raan4Real" target="_blank">Twitter</a></li>
                            <li><a href="https://www.facebook.com/AbstractMindedOfficial" target="_blank">Facebook</li>
                            <li><a href="https://www.instagram.com/abstractminded_band/" target="_blank">Instagram</li>
                        </ul>
                    </div>

                    <div class="plateforme_ecoute col-lg-6">
                        <h4>Ecoutez-moi sur vos plateformes préférées!</h4>
                        <ul>
                            <li><a href="https://www.youtube.com/channel/UCSNg4WjCmuSJK3q5VZ7_oPA" target="_blank">Youtube</li>
                            <li><a href="https://open.spotify.com/artist/6WD3yYKgB4HtOPBHw9eBBo" target="_blank">Spotify</li>
                            <li><a href="https://www.deezer.com/fr/artist/51804982" target="_blank">Deezer</li>
                        </ul>
                    </div>
                </div>
            </div>
        </footer>

        <!-- rajout de la librairie js -->
        <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
        <script type="text/javascript" src="js/app.js"></script>
    </body>
</html>