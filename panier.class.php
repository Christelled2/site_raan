<?php
class panier {
    public function __construct() {
        //s'il n'y a pas de session ouverte, on en créer une
        if(!isset($_SESSION)) {
            session_start();
        }

        //s'il n'y a pas de tableau panier créé, on en créé un
        if(!isset($_SESSION['panier'])) {
            $_SESSION['panier'] = array();
        }
    }   

    public function add($product_id){
        if (isset($_SESSION['panier'][$product_id])) {
            $_SESSION['panier'][$product_id]++;
        } else {
            $_SESSION['panier'][$product_id] = 1;
        }
    }

    public function del($product_id) {
        unset($_SESSION['panier'][$product_id]);
    }

    public function minus($product_id) {
        if (($_SESSION['panier'][$product_id]) == 1) {
            unset($_SESSION['panier'][$product_id]);
        } else {
            $_SESSION['panier'][$product_id]--;
        }
    }

    public function plus($product_id) {
        $_SESSION['panier'][$product_id]++;
    }

    public function nombre_element_panier() {
        if (array_sum($_SESSION['panier']) > 1) {
            return array_sum($_SESSION['panier']) . " éléments";
        } else {
            return array_sum($_SESSION['panier']) . " élément";
        }
    }
}
?>