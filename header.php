<?php
require 'db.class.php';
$db = new db();
require 'panier.class.php';
$panier = new panier();

/*try {
    $bdd = new PDO('mysql:host=localhost;dbname=raan;charset=utf8', 'root', '', array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));
} catch(Exception $e) {
        die('Erreur : '.$e->getMessage());
}*/

?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <link rel="stylesheet" type="text/css" href="style/style.css">
        <link rel="stylesheet" href="style/bootstrap.css">
        <link href="https://fonts.googleapis.com/css?family=Permanent+Marker&display=swap" rel="stylesheet"> 
    </head>
    <body>
        
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

        <header>
            <div> <img class="logo" src="./pic/raan_logo.png" alt="logo" /> </div>
            <nav>
                <ul>
                    <li class="<?php if(basename($_SERVER['PHP_SELF']) == "index.php") { echo "page_actuelle"; } ?>" > <a href="index.php">Accueil</a> </li>
                    <li class="<?php if(basename($_SERVER['PHP_SELF']) == "merch.php") { echo "page_actuelle"; } ?>" > <a href="merch.php">Merch</a> </li>
                    <li class="<?php if(basename($_SERVER['PHP_SELF']) == "biographie.php") { echo "page_actuelle"; } ?>" > <a href="biographie.php">Biographie</a> </li>
                    <li class="<?php if(basename($_SERVER['PHP_SELF']) == "media.php") { echo "page_actuelle"; } ?>" > <a href="media.php">Media</a> </li>
                    <li class="<?php if(basename($_SERVER['PHP_SELF']) == "contact.php") { echo "page_actuelle"; } ?>" > <a href="contact.php">Contact</a> </li>
                    <li class="<?php if(basename($_SERVER['PHP_SELF']) == "panier.php") { echo "page_actuelle"; } ?>" > <a href="panier.php">Panier</a> </li>
                </ul>
            </nav>
        </header>
    </body>
</html>