<?php include("header.php"); ?>
<!DOCTYPE html>
<html>
    <head>
        <title>RAAN - CONTACT</title>
    </head>

    <body>

    <h2>Avez-vous quelque chose à me dire?</h2>

    <section class="contact_form d-flex justify-content-center">
        <form method="post" action="traitement.php">
            <p>Votre email:</p>
            <input type="text" name="email" size="35" placeholder="Votre email..."/>
            </br></br>

            <p>Sujet:</p>
            <input type="text" name="sujet" size="35" placeholder="Sujet..."/>
            </br></br>

            <p>Votre message:</p>
            <textarea class="form-control z-depth-1" name="message" rows="6" cols="60" placeholder="Vous pouvez taper votre message ici ..."></textarea>
            </br></br></br>
            <input class="bouton_submit btn btn-primary center-block" type="submit" value="Envoyer"/>
        </form>
    </section>
  
    <?php include("footer.php"); ?>
    </body>
</html>